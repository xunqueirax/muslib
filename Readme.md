Utility to normalize the names of a directory tree according to some rules, 
such as no spaces, or no upper case letters.

It also allows to create m3u paylists for each directory that contain audio.

MusDo.py help shows:

Usage: MusDo.py [options]

Options:
  -h, --help            show this help message and exit
  -d DIR, --directory=DIR
                        the directory to process
  -n, --normalize       whether to normalize file names in directory
  -l, --lists           whether to write playlists


