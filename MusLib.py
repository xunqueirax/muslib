#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

class StrUtl:
	
	def normalize(self, unnormal, filters=None):
		if filters is None:
			filters = ["lower", "und", "spacedash", "special", "accent"]
		for f in filters:
			if f == 'lower':
				unnormal = unnormal.lower()
			if f == 'und':
				unnormal = unnormal.replace(' ', '_')
			if f == 'spacedash':
				unnormal = unnormal.replace("–", "-")
				unnormal = unnormal.replace('_-_', '-')
				unnormal = unnormal.replace('___', '-')
				unnormal = unnormal.replace('__', '-')
			if f == 'special':
				unnormal = unnormal.replace('"', '')
				unnormal = unnormal.replace(',', '')
				unnormal = unnormal.replace("'", "")
			if f == 'accent':
				unnormal = unnormal.replace('á', 'a')
				unnormal = unnormal.replace('à', 'a')
				unnormal = unnormal.replace("é", "e")
				unnormal = unnormal.replace("é", "e")
				unnormal = unnormal.replace("í", "i")
				unnormal = unnormal.replace("ì", "i")
				unnormal = unnormal.replace("ó", "o")
				unnormal = unnormal.replace("ò", "o")
				unnormal = unnormal.replace("ú", "u")
				unnormal = unnormal.replace("ù", "u")
				unnormal = unnormal.replace("ä", "ae")
				unnormal = unnormal.replace("ë", "e")
				unnormal = unnormal.replace("ï", "i")
				unnormal = unnormal.replace("ö", "oe")
				unnormal = unnormal.replace("ü", "ue")
		return unnormal

class MusicLib:
	
	strUtl = StrUtl()
	
	
	def normalizePath(self, root, name, dryRun=False):
		normal = self.strUtl.normalize(name)
		if normal != name:
			if root is not None:
				path = os.path.join(root, name)
				nuPath = os.path.join(root, normal)
			else:
				path = name
				nuPAth = normal
			
			if os.path.isdir(nuPath):
				print ("at " + root + ", " + normal + " exists. Will copy contents from " + name)
				if not dryRun:
					for subitem in os.listdir(path):
						print ("Will copy " + os.path.join(path, subitem) + " to " + os.path.join(nuPath, subitem))
						os.rename(os.path.join(path, subitem), os.path.join(nuPath, subitem))
			else:
				print ("at " + root + " will change " + name + " ----> " + normal)
				if not dryRun:
					os.rename(path, nuPath)
	
	def normalizeTree(self, aPath=None):
		if aPath is None:
			aPath = os.getcwd()
		elif os.path.isfile(aPath):
			self.normalizePath(None, aPath)
		
		for root, dirs, files in os.walk(aPath, False):
			for f in files:
				self.normalizePath(root, f)
			for f in dirs:
				self.normalizePath(root, f)
	
	def rebuildM3u(self, aPath=None):
		if aPath is None:
			aPath = os.getcwd()
		
		if os.path.isdir(aPath):
			for root, dirs, files in os.walk(aPath, False):
				for f in files:
					if f.endswith("m3u"):
						os.remove(os.path.join(root, f))
			
			for root, dirs, files in os.walk(aPath, False):
				base = os.path.basename(root)
				musicfiles = []
				for f in files:
					if f.endswith(("mp3", "mp4", "ogg", "flac", "wav")):
						musicfiles.append(os.path.join(base, f))
				if len(musicfiles) > 0:
					musicfiles.sort()
					m3uPath = os.path.join(root, os.path.join("..", base + ".m3u"))
					m3uFile = open(m3uPath, "w")
					for line in musicfiles:
						m3uFile.write(line + "\n")
					m3uFile.close()

