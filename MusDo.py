#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from MusLib import *
from optparse import OptionParser

# Parse parameters ##################
parser = OptionParser()
parser.add_option("-d", "--directory", action="store",      dest="dir",   help="the directory to process", metavar="DIR")
parser.add_option("-n", "--normalize", action="store_true", dest="norm",  help="whether to normalize file names in directory")
parser.add_option("-l", "--lists",     action="store_true", dest="lists", help="whether to write playlists")
(options, args) = parser.parse_args()
#####################################

mLib = MusicLib()

if options.norm:
	print ("Will normalize " + options.dir)
	mLib.normalizeTree(options.dir)

if options.lists:
	print ("Will create playlists in " + options.dir)
	mLib.rebuildM3u(options.dir)
